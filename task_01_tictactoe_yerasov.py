# -*- coding: cp65001 -*-
from pprint import pprint
import random

X, O, _ = 1, 0, None
cellStates = X, O, _

#Примеры состояний игрового поля
# Побеждает О
TEST_BOARD_OWIN = (
    O, X, O,
    X, O, X,
    O, _, X
)
# Побеждает Х
TEST_BOARD_XWIN = (
    O, X, O,
    X, X, O,
    _, X, _
)
# Игра еще не окончена
TEST_BOARD_UNDEFINED = (
    O, X, O,
    X, O, X,
    _, _, X
)
# Ничья
TEST_BOARD_DRAW = (
    O, X, O,
    X, O, X,
    X, O, X
)
# Неправильно заданная доска
TEST_BOARD_BAD = (
    O, X, 4,
    1, 2, X,
    X, ")", X
)

# 4 варианта исхода игры
O_WINS, X_WINS, UNDEFINED, DRAW, WRONG_BOARD = range(5)

# Условия победы для Х и для О
O_WIN_STATE = (O, O, O)
X_WIN_STATE = (X, X, X)

# Вывести доску на экран
def printBoard(board):
    Row = ""
    i = 0
    for cell in board :
        Row += cellFormat(cell) + " "
        i+=1
        if (i == 3) :
            print(Row)
            i -= 3
            Row = ""
        
# Заменить число на символ
def cellFormat(cell):
    return {
        X: "X",
        O: "O",
        _: "_",
    }.get(cell, str(cell))

# Вернуть выигрышные комбинации
def slice3(board):
    return ((board[0:3]),
            (board[3:6]),             
            (board[6:9]),   			
            (board[0::3]),                            
            (board[1::3]),                             
            (board[2::3]),                             
            (board[0::4]),  
            (board[2:8:2]))  
                            
# Определить и вернуть состояние поля
def outcome(board):
    if validateCells(board):
        for boardSlice in slice3(board) :
            if (boardSlice == O_WIN_STATE):
                return O_WINS
            elif (boardSlice == X_WIN_STATE):
                return X_WINS
        if (_ in board):
            return UNDEFINED
        return DRAW
    else: return WRONG_BOARD

# Проверить синтаксис элементов на поле
def validateCells(board):
    for cell in board :
            if not (cell in cellStates):
                 return False
    return True

# Вернуть состояниe поля      
def decodeResult(result):
    return {
        O_WINS: "O Wins",
        X_WINS: "X Wins",
        DRAW: "Draw",
        UNDEFINED: "Undefined",
        WRONG_BOARD: "Wrong elements on board"
    }.get(result, "Unexpected result type") 

# Вернуть правильное случайное поле
def generateValidRandomBoard():
    newBoard = []
    symbols = [X, O, _]
    i = 0
    for i in range (9):
        random.shuffle(symbols)
        newBoard.append(symbols[0])
    if (newBoard.count(X) > newBoard.count(O) + 1):
        pass
    elif (newBoard.count(O) > newBoard.count(X) + 1):
        pass
    return tuple(newBoard)

def main():
    print("TEST_BOARD_OWIN")
    printBoard(TEST_BOARD_OWIN)
    print(decodeResult(outcome(TEST_BOARD_OWIN)), "\n")

    print("TEST_BOARD_XWIN")
    printBoard(TEST_BOARD_XWIN)    
    print(decodeResult(outcome(TEST_BOARD_XWIN)), "\n")

    print("TEST_BOARD_UNDEFINED")
    printBoard(TEST_BOARD_UNDEFINED)
    print(decodeResult(outcome(TEST_BOARD_UNDEFINED)), "\n")

    print("TEST_BOARD_DRAW")
    printBoard(TEST_BOARD_DRAW)
    print(decodeResult(outcome(TEST_BOARD_DRAW)), "\n")

    print("TEST_BOARD_BAD")
    printBoard(TEST_BOARD_BAD)
    print(decodeResult(outcome(TEST_BOARD_BAD)), "\n")

    for j in range (3) :
        print("Random Board " + str(j+1))
        randomBoard = generateValidRandomBoard()
        printBoard(randomBoard) 
        print(decodeResult(outcome(randomBoard)), "\n")
        
main()

