ROMAN_NUMBERS = {
    1: "I",
    5: "V",
    10: "X",
    50: "L",
    100: "C",
    500: "D",
    1000: "M"
    }

# 3999 MMMCMXCIX

def int2roman(num):
    copy = num
    denoms = list(ROMAN_NUMBERS.keys())
    rom = ""
    for i in range(len(denoms) - 1, -1, -1):
        if ((num // denoms[i] == 0) and (num % denoms[i] == num)):
            pass
        elif (str(num)[0]== '9'):
                rom += (ROMAN_NUMBERS[denoms[i-1]]) + (ROMAN_NUMBERS[denoms[i+1]])
                num -= (num // denoms[i-1]) * denoms[i-1]
        elif (str(num)[0]== '4'):
                rom += (ROMAN_NUMBERS[denoms[i]]) + (ROMAN_NUMBERS[denoms[i+1]])
                num -= (num // denoms[i]) * denoms[i]
        else:
            for x in range (0, (num // denoms[i])):
                rom += (ROMAN_NUMBERS[denoms[i]])
            num -= (num // denoms[i]) * denoms[i]
    print(copy, "=", rom)

def main(*args):
    for x in range (1, 4000):
        int2roman(x)
    
main()
